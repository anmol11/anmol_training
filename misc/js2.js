class Person{
    constructor(name,age,salary,sex)
    {
        this.name=name;
        this.age=age;
        this.salary=salary;
        this.sex=sex;
    }

}
var Parr={};
Parr[0]=new Person("Anmol",22,12000,'M');
Parr[1]=new Person("Kritika",22,15000,'F');
Parr[2]=new Person("Monty",25,22000,'M');
Parr[3]=new Person("Aastha",24,42000,'F');


var result=quickSort(Parr,0,Parr.length-1,"name");

console.log(Parr);
console.log(result);
var items = [5,3,7,6,2,9];
    function swap(items, leftIndex, rightIndex){
        var temp = items[leftIndex];
        items[leftIndex] = items[rightIndex];
        items[rightIndex] = temp;
    }
    function compare( a, b ) {
        if ( a.last_nom < b.last_nom ){
          return -1;
        }
        if ( a.last_nom > b.last_nom ){
          return 1;
        }
        return 0;
      }
    function partition(items, left, right,param) {
        var pivot   = items[Math.floor((right + left) / 2)], //middle element
            i       = left, //left pointer
            j       = right; //right pointer
        while (i <= j) {
            while (items[i]< pivot) {
                i++;
            }
            while (items[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(items, i, j); //sawpping two elements
                i++;
                j--;
            }
        }
        return i;
    }
    
    function quickSort(items, left, right) {
        var index;
        if (items.length > 1) {
            index = partition(items, left, right); //index returned from partition
            if (left < index - 1) { //more elements on the left side of the pivot
                quickSort(items, left, index - 1);
            }
            if (index < right) { //more elements on the right side of the pivot
                quickSort(items, index, right);
            }
        }
        return items;
    }
    // first call to quick sort
    //var sortedArray = quickSort(items, 0, items.length - 1);
    //console.log(sortedArray); //prints [2,3,5,6,7,9]