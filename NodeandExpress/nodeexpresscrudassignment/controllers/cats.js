import { v4 as uuidv4 } from "uuid";

let cats = [];

export const getAllCats = (req, res) => {
    res.send(cats);
}

export const getCat = (req, res) => {
    const { id: catId } = req.params;
    const cat = cats.find(val => val.id === catId);
    res.status(200).send(cat);
}

export const createCat = (req, res) => {
    const catData = req.body;
    cats.push({...catData, id: uuidv4()});
    res.status(201).send(`cat with name: ${catData.name} was added to the db.`);
}

export const deleteCat = (req, res) => {
    const { id: catId } = req.params;
    cats = cats.filter(val => val.id !== catId);
    res.status(200).send(`cat with id: ${catId} was deleted from the db.`)
}

export const updateCat = (req, res) => {
    const { id: catId } = req.params;
    const { name, age, breed } = req.body;
    const cat = cats.find(val => val.id === catId);
    cat.name = name;
    cat.age = age;
    cat.breed = breed;
    res.send(`cat with id: ${catId} updated successfully.`);
}