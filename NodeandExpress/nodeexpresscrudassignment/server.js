import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';

import catsRoutes from "./routes/cats.js";

const app = express();

const PORT = process.env.PORT || 5000;

// middlewares
app.use(bodyParser.json());
app.use(morgan("tiny"));

// routes middlewares
app.use("/cats", catsRoutes);

// starting server
app.listen(PORT, () => console.log(`Server running at http://localhost:${PORT}`));