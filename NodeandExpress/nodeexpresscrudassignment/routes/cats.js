import express from "express";
import { getAllCats, getCat, createCat, deleteCat, updateCat} from "../controllers/cats.js";

const router = express.Router();

// cats routes
router.get("/", getAllCats);

router.get("/:id", getCat);

router.post("/", createCat);

router.delete("/:id", deleteCat);

router.put("/:id", updateCat);

export default router;