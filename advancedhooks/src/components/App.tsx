import React from 'react';
import './App.css';
import AdvancedHooksComponent from "./components/AdvancedHooksComponent";

const App: React.FC = () => {
  return (
    <div className="App">
        <AdvancedHooksComponent/>
    </div>
  );
};

export default App;